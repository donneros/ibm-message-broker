import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import com.ibm.broker.config.proxy.BrokerProxy;
import com.ibm.broker.config.proxy.ConfigManagerProxyLoggedException;
import com.ibm.broker.javacompute.MbJavaComputeNode;
import com.ibm.broker.plugin.*;


public class GatewayFlow_JavaCompute extends MbJavaComputeNode {

	private BrokerProxy bp = null; 
	//Map to store operation and URL
	private Map<String, String> operationToURL = Collections.synchronizedMap(new HashMap<String, String>());

	public GatewayFlow_JavaCompute() throws ConfigManagerProxyLoggedException {
		super();
		bp = BrokerProxy.getLocalInstance();
	}
	
	@Override
	protected void finalize() throws Throwable {
		super.finalize();
		if (bp!=null)
		{
			bp.disconnect();
		}
	}
		
	public void evaluate(MbMessageAssembly assembly) throws MbException {
		MbOutputTerminal out = getOutputTerminal("out");

		MbMessage message = assembly.getMessage();
		
		//Get the local environment
		MbMessage le = assembly.getLocalEnvironment();
		
		//Get the operation name
		MbElement element = message.getRootElement().getFirstElementByPath("SOAP/Context/operation");
		String operation=element.getValueAsString();
		
	    //Find the URL 
		String URL = getURL(operation);
		if (URL==null)
		{
			//Throw an MbUserException
			throw new MbUserException(this.getName(), "evaluate", "GatewayError", "9999", "Can't find URL for service operation " + operation, null);
		}
		
		//Set the WebServiceURL creating the path if it doesn't exist
		le.getRootElement().evaluateXPath("?Destination/?SOAP/?Request/?Transport/?HTTP/?WebServiceURL[set-value('"+URL+"')]");
		
		// The following should only be changed
		// if not propagating message to the 'out' terminal
		out.propagate(assembly);
	}
	
	private String getURL(String operation) throws MbUserException
	{
		//Attempt to get URL from the Map
		String URL = operationToURL.get(operation);
		//If the URL is null then the operation is new, 
		//so look up the configurable service property
		if (URL==null)
		{
			//Check broker proxy
			if (bp==null)
			{
				//Throw an MbUserException
				throw new MbUserException(this.getName(), "getURL", "GatewayError", "9999", "BrokerProxy object is null at runtime", null);
			}
			//Get the URL
			try {
				URL = bp.getConfigurableServiceProperty("UserDefined/Gateway/"+operation);
			} catch (Exception e) {
				//Handle null URL elsewhere
				URL=null;
			}
			//Store the URL in our hashmap if found
			if (URL!=null) 
			{
				operationToURL.put(operation, URL);
			}
		}
		return URL;
	}

}
