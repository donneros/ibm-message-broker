***************************************************
*  Run the following command to create the queues *
*  runmqsc <queue manager name> <MB8BROKER_JSONRESTSampleExecutionGroup.tst *
***************************************************
DEFINE QL('APP_OUT1')
DEFINE QL('UPDATE_IN1')
DEFINE QL('XML_CONS_IN1')
DEFINE QL('JSONSVC_STATE1')
DEFINE QL('DELETE_IN1')
DEFINE QL('CREATE_IN1')
DEFINE QL('RETRIEVE_IN1')
DEFINE QL('XML_CONS_REPLY1')
DEFINE QL('JSON_REPLY1')
DEFINE QL('APP_IN1')
